/* 
  Example with input : key=0, val=('Bob,John,Paul')
  --> [{
    key : 0,
    value : {
      num : 0,
      finalValue : "Bob"
    }
  },{
    key : 0,
    value : {
      num : 1,
      finalValue : "John"
    }
  },{
    key : 0,
    value : {
      num : 2,
      finalValue : "Paul"
    }
  }] will be added to context.
*/
function Map(key, val, context=[]){
  let line = val.split(",")
  for(key2 in line){
    context.push({
      key : key2,
      value : {
        num : key,
        finalValue : line[key2]  
      }
    })
  }
}

/* 
  Example with input : key=0, val=[{num : 1, val : "Dog"}, {num : 0, val : "John"}]
  --> [{
    key : 0,
    value : [John, Dog]
  }] will be added to context.
*/
function Reduce(key, val, context=[]){
  let val2 = []
  for (key in val){
    val2[val[key].num] = val[key].finalValue
  }
  context.push({
    key : key,
    value : val2
  })
}